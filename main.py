import asyncio
import os

from config import PRODUCERS_COUNT, CONSUMERS_COUNT, DEFAULT_FILENAME
from core.consumer import Consumer
from core.producer import Producer


class Scraper:
    def __init__(self, producers_count: int, consumers_count: int, **producer_kwargs):
        self.producers_count = producers_count
        self.consumers_count = consumers_count
        self.producer_kwargs = producer_kwargs
        self.queue = asyncio.Queue()

    async def run(self):
        producers = self.create_coroutines(self.create_producer, self.producers_count, **self.producer_kwargs)
        consumers = self.create_coroutines(self.create_consumer, self.consumers_count)

        await asyncio.gather(*producers)
        await self.queue.join()

        for consumer in consumers:
            consumer.cancel()

    @staticmethod
    def create_coroutines(function, count: int, **kwargs) -> list:
        return [asyncio.create_task(function(**kwargs))
                for _ in range(count)]

    async def create_producer(self, **kwargs):
        producer = Producer(self.queue)
        await producer.produce(**kwargs)

    async def create_consumer(self):
        while True:
            print('main')
            task = await self.queue.get()
            consumer = Consumer()
            await consumer.consume(task)
            self.queue.task_done()


if __name__ == '__main__':
    filename = os.getenv('FILENAME', DEFAULT_FILENAME)
    scraper = Scraper(PRODUCERS_COUNT, CONSUMERS_COUNT, filename=filename)

    # asyncio.run(scraper.run())
    loop = asyncio.get_event_loop()
    loop.run_until_complete(scraper.run())
