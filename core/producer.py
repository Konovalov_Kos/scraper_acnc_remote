import asyncio
import csv


class Producer:

    def __init__(self, queue: asyncio.Queue):
        self.queue = queue

    async def produce(self, filename: str):
        with open(filename, 'r') as file:
            csv_reader = csv.reader(file)

            for line in csv_reader:

                if line[0].isdigit():
                    print('producer')
                    task = {
                        'abn': line[0]
                    }
                    await self.queue.put(task)
