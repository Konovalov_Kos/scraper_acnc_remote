import re
from typing import Iterable, Optional
from urllib.parse import urljoin

from httpx import Response
from more_itertools import unique_everseen
from parsel import Selector
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from config import DOMAIN, HEADERS, PROXIES, POSTGRESQL_CONNECTION, DEFAULT_RETRIES
from modules.fetcher import Fetcher
from database_helper.models import Base, BusinessData


class Consumer:
    _URL_TO_BUSINESS_CONTAINER = 'https://www.acnc.gov.au/charity?name_abn%5B0%5D={}'

    _UPPER = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    _LOWER = 'abcdefghijklmnopqrstuvwxyz'
    _IMG_SRC_IDENTIFIER = 'http'

    _ICON_POTENTIAL_ATTRIBUTE = ['src', 'alt', 'id', 'class']
    _ICON_POTENTIAL_ATTRIBUTE_PARTS = ['logo', 'icon']

    _RE_WHITESPACES = re.compile(r'\s+')
    _RE_ICON_URL_IN_STYLE = re.compile(r':url\((.*?)\)')

    _ICON_IN_HREF_BLOCK_XPATH = '//a[@href="/" or href="#" or @href="{}"]//img/@src'
    _BUSINESS_LINK_XPATH = './/tbody//td[contains(@class, "title")]//a/@href'
    _ABN_XPATH = './/div[contains(@class, "abn")]//a/text()'
    _EMAIL_XPATH = './/div[contains(@class, "name-field-email")]//a/text()'
    _WEBSITE_XPATH = './/div[contains(@class, "name-field-website")]//a/text()'
    _PHONE_XPATH = './/div[contains(@class, "name-field-phone")]//a/text()'
    _SUMMARY_OF_ACTIVITIES_ENTRIES_XPATH = '//div[contains(@class, "name-field-activities")]//text()'
    _BUSINESS_NAME_XPATH = './/h1[@class="page-header"]/text()'
    _DISPLAY_NAME_ENTRIES_XPATH = './/div[contains(@class, "alternate-names")]//div[@class="field-items"]//text()'
    _STREET_BLOCK_ENTRIES_XPATH = '//div[@class="street-block"]//text()'
    _LOCALITY_BLOCK_ENTRIES_XPATH = '//div[contains(@class, "locality-block")]//text()'
    _COUNTRY_XPATH = './/span[@class="country"]/text()'

    def __init__(self):
        self.engine = create_engine(POSTGRESQL_CONNECTION)
        Base.metadata.create_all(self.engine)
        self.session = Session(bind=self.engine)
        self.fetcher = Fetcher()

    async def _fetcher_callback_request(self,
                                        method: str,
                                        url: str,
                                        callback: str,
                                        meta: dict = None,
                                        **kwargs) -> (Response, dict):
        response = await self.fetcher.request(method, url, **kwargs)
        await getattr(self, callback)(response, meta)

    async def consume(self, task: dict) -> None:
        print(1111111111)
        business_abn = task['abn']
        url = urljoin(self._URL_TO_BUSINESS_CONTAINER, business_abn)
        await self._fetcher_callback_request('GET',
                                             url,
                                             headers=HEADERS,
                                             proxies=PROXIES,
                                             timeout=60,
                                             allowed_http_codes=[200],
                                             max_retries=DEFAULT_RETRIES,
                                             callback='_make_request_to_business_page',
                                             meta={'search_page_url': url})

    async def _make_request_to_business_page(self, response: Response, meta: Optional[dict]) -> None:
        tree = Selector(response.text)
        current_page = meta['search_page_url']
        business_url_part = tree.xpath(self._BUSINESS_LINK_XPATH).extract_first()

        if not business_url_part:
            raise ValueError(f'The business link is not found. Page: {current_page}')

        business_url = urljoin(DOMAIN, business_url_part)
        await self._fetcher_callback_request('GET',
                                             business_url,
                                             headers=HEADERS,
                                             proxies=PROXIES,
                                             timeout=60,
                                             allowed_http_codes=[200],
                                             max_retries=DEFAULT_RETRIES,
                                             callback='_extract_business_data',
                                             meta={'business_url': business_url})

    async def _extract_business_data(self, response: Response, meta: Optional[dict]) -> None:
        tree = Selector(response.text)
        business_url = meta['business_url']
        abn = self._get_abn(tree)
        business_name = self._get_business_name(tree)
        display_name = self._get_display_name(tree)
        address = self._get_address(tree)
        email = self._get_email(tree)
        website = self._get_website(tree)
        phone = self._get_phone(tree)
        summary_of_activities = self._extract_summary_of_activities(tree)

        business_id = await self._save_business_to_db(
            business_url=business_url,
            abn=abn,
            business_name=business_name,
            display_name=display_name,
            address=address,
            email=email,
            website=website,
            phone=phone,
            summary_of_activities=summary_of_activities,
            logo_src=None
        )

        await self._find_logo_src_if_exists(website, business_id)

    def _extract_summary_of_activities(self, selector: Selector) -> Optional[str]:

        if selector:
            summary_of_activities_entries = selector.xpath(self._SUMMARY_OF_ACTIVITIES_ENTRIES_XPATH).extract()

            cleaned_summary_of_activities_entries = [entry.strip() for \
                                                     entry in summary_of_activities_entries if entry.strip()]

            summary_of_activities = '\n'.join(cleaned_summary_of_activities_entries)

            if not summary_of_activities:
                summary_of_activities = None

            return summary_of_activities

    async def _find_logo_src_if_exists(self, url: str, business_id: int) -> None:

        if not url:
            return

        if not url.startswith('http'):
            url = 'https://' + url

        await self._fetcher_callback_request('GET',
                                             url,
                                             headers=HEADERS,
                                             proxies=PROXIES,
                                             timeout=60,
                                             allowed_http_codes=[200],
                                             max_retries=DEFAULT_RETRIES,
                                             callback='_extract_icon',
                                             meta={'business_id': business_id, 'website_url': url})

    async def _extract_icon(self, response: Response, meta: Optional[dict]) -> None:
        tree = Selector(response.text)
        business_id = meta['business_id']
        website_url = meta['url']
        icon_urls = self._extract_icon_in_href_block(tree, website_url)

        if not icon_urls:
            icon_blocks = self._extract_icon_blocks(tree)
            image_blocks = filter(lambda block: block[0] == 'img', icon_blocks)
            non_image_blocks = filter(lambda block: block[0] != 'img', icon_blocks)
            icon_urls = self._extract_icon_from_images(image_blocks)

            if not icon_urls:
                icon_urls = self._extract_icon_from_non_images(non_image_blocks)

        icon_urls = list(unique_everseen(icon_urls))

        if icon_urls:
            icon_url = icon_urls[0]
            icon_url = urljoin(website_url, icon_url)
        else:
            icon_url = None

        await self._save_business_logo_to_db(icon_url, business_id)

    def _extract_icon_in_href_block(self, tree: Selector, url: str) -> list:
        return tree.xpath(self._ICON_IN_HREF_BLOCK_XPATH.format(url)).extract()

    def _extract_icon_blocks(self, tree: Selector) -> list:
        icon_blocks = []
        for attribute in self._ICON_POTENTIAL_ATTRIBUTE:
            for path in self._ICON_POTENTIAL_ATTRIBUTE_PARTS:
                xpath = f'//*[contains(translate(@{attribute}, ' \
                        f'"{self._UPPER}", "{self._LOWER}"), "{path}")]'
                blocks = tree.xpath(xpath)
                for block in blocks:
                    name = block.xpath('name()').extract_first()
                    icon_blocks.append((name, block))
        return icon_blocks

    def _extract_icon_from_images(self, blocks: Iterable) -> list:
        icon_urls = []

        for _, block in blocks:
            self.__append_by_xpath(block, '@src', icon_urls)

        return icon_urls

    def _extract_icon_from_non_images(self, blocks: Iterable) -> list:
        icon_urls = []

        for _, block in blocks:
            self.__append_by_xpath(block, './img/@src', icon_urls)
            self.__append_by_xpath(block, './*/img/@src', icon_urls)
            style = block.xpath('@style').extract_first()
            if style:
                style = self._RE_WHITESPACES.sub('', style)
                if ':url(' in style:
                    icon_urls_current = self \
                        ._RE_ICON_URL_IN_STYLE.findall(style)
                    icon_urls.extend(icon_urls_current)

        return icon_urls

    async def _save_business_to_db(self, **kwargs) -> int:

        try:
            business_data = BusinessData(
                business_url=kwargs['business_url'],
                abn=kwargs['abn'],
                business_name=kwargs['business_name'],
                display_name=kwargs.get('display_name'),
                address=kwargs.get('address'),
                email=kwargs['email'],
                website=kwargs['website'],
                phone=kwargs['phone'],
                summary_of_activities=kwargs['summary_of_activities'],
                logo_src=kwargs['logo_src']
            )
            self.session.add(business_data)
            self.session.commit()
            return business_data.id

        except Exception as e:
            print(e)
            self.session.rollback()

    async def _save_business_logo_to_db(self, logo_src: str, business_id: int) -> None:

        if logo_src and self._IMG_SRC_IDENTIFIER in logo_src:

            try:
                business = self.session.query(BusinessData).filter_by(id=business_id).one_or_none()
                business.logo_src = logo_src
                self.session.commit()

            except Exception as e:
                print(e)
                self.session.rollback()

    def _get_phone(self, tree: Selector) -> Optional[str]:
        phone = tree.xpath(self._PHONE_XPATH).extract_first()
        return self.__strip_value_if_exists(phone)

    def _get_website(self, tree: Selector) -> Optional[str]:
        website = tree.xpath(self._WEBSITE_XPATH).extract_first()
        return self.__strip_value_if_exists(website)

    def _get_email(self, tree: Selector) -> Optional[str]:
        email = tree.xpath(self._EMAIL_XPATH).extract_first()
        return self.__strip_value_if_exists(email)

    def _get_business_name(self, tree: Selector) -> Optional[str]:
        business_name = tree.xpath(self._BUSINESS_NAME_XPATH).extract_first()
        return self.__strip_value_if_exists(business_name)

    def _get_abn(self, tree: Selector) -> Optional[str]:
        abn = tree.xpath(self._ABN_XPATH).extract_first()
        return self.__strip_value_if_exists(abn)

    def _get_address(self, tree: Selector) -> Optional[str]:

        street_entries = tree.xpath(self._STREET_BLOCK_ENTRIES_XPATH).extract()
        locality_entries = tree.xpath(self._LOCALITY_BLOCK_ENTRIES_XPATH).extract()
        country = tree.xpath(self._COUNTRY_XPATH).extract_first()
        country = country.strip() if country else ''

        cleaned_street_entries = [entry.strip() for entry \
                                  in street_entries if entry.strip()]

        cleaned_locality_entries = [entry.strip() for entry \
                                    in locality_entries if entry.strip()]

        street_address = ' '.join(cleaned_street_entries)
        locality_address = ' '.join(cleaned_locality_entries)

        cleaned_full_address_entries = [entry.strip() for entry in \
                                        [street_address, locality_address,
                                         country] if entry.strip()]

        full_address = ', '.join(cleaned_full_address_entries)
        return full_address

    def _get_display_name(self, tree: Selector) -> Optional[str]:
        display_name_entries = tree.xpath(self._DISPLAY_NAME_ENTRIES_XPATH).extract()
        cleaned_display_name_entries = [entry.strip() for entry in display_name_entries if entry.strip()]

        if not cleaned_display_name_entries:
            return None

        display_name = ', '.join(cleaned_display_name_entries)
        return self.__strip_value_if_exists(display_name)

    @staticmethod
    def __strip_value_if_exists(value: str):

        if value:
            return value.strip()

    @staticmethod
    def __append_by_xpath(element: Selector, xpath: str, lst: list):
        values = element.xpath(xpath).extract()
        lst.extend(values)
