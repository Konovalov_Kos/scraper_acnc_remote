from sqlalchemy import Column, String, Integer, Text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class BusinessData(Base):
    __tablename__ = 'business_data'
    id = Column(Integer, primary_key=True)
    business_url = Column(String(1000))
    abn = Column(String(20), nullable=False)
    business_name = Column(String(300), nullable=False)
    display_name = Column(Text(), nullable=True)
    address = Column(String(600), nullable=True)
    email = Column(String(100), nullable=True)
    website = Column(String(255), nullable=True)
    phone = Column(String(100), nullable=True)
    summary_of_activities = Column(Text(), nullable=True)
    logo_src = Column(String(2000), nullable=True)
