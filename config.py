# Fetcher
DEFAULT_RETRIES = 10
DEFAULT_TIMEOUT = 30

# Scraper
PRODUCERS_COUNT = 1
CONSUMERS_COUNT = 10

# Custom
DEFAULT_FILENAME = 'input.csv'
DOMAIN = 'https://www.acnc.gov.au'
POSTGRESQL_CONNECTION = 'postgresql+psycopg2-binary://User:asfuasfo1adp8am@167.86.124.5:5433/acnc_async'
HEADERS = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/84.0.4147.105 Safari/537.36 OPR/70.0.3728.119',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,'
              '*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'accept-language': 'en-US,en;q=0.9'
}
PROXIES = {'http': 'http://5.189.151.227:24059',
           'https': 'http://5.189.151.227:24059'}
