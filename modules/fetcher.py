import httpx
from httpx import Response

from config import DEFAULT_RETRIES, DEFAULT_TIMEOUT


class Fetcher:
    async def request(self,
                      method,
                      url,
                      allow_status_codes: list = None,
                      retries: int = DEFAULT_RETRIES,
                      **kwargs) -> Response:
        proxies = kwargs.pop('proxies', None)
        timeout = kwargs.pop('timeout', DEFAULT_TIMEOUT)
        for order in range(retries + 1):
            try:
                async with httpx.AsyncClient(proxies=proxies, verify=False, timeout=timeout) as client:
                    response = await client.request(method, url, **kwargs)
                    if allow_status_codes:
                        self.validate_response_status_codes(response, allow_status_codes)
                return response
            except Exception as e:
                if order == retries:
                    raise e

    @staticmethod
    def validate_response_status_codes(request: Response, allow_status_codes: list):
        assert request.status_code in allow_status_codes
